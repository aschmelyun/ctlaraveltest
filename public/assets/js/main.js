$('#add_item').click(function(e) {
    e.preventDefault();

    $.ajax({
        method: "POST",
        url: '/',
        data: {
            product_name: $('#product_name').val(),
            quantity_in_stock: $('#quantity_in_stock').val(),
            price_per_item: $('#price_per_item').val()
        }
    })
        .done(function(msg) {
            if(msg == 'success') {
                console.log('success');
                $('#products_table').load(document.URL + ' #products_table');
            }
        });
});