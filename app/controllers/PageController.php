<?php
    
class PageController extends BaseController {
    
    public function index() {

        return View::make('form', array(
            'product_json'  => json_decode(file_get_contents(storage_path() . '/products.json')),
            'total_value'   => 0
        ));
    }

    public function saveForm() {
        $path = storage_path() . '/products.json';

        $current_json = file_get_contents($path);
        $current_data = json_decode($current_json);
        $current_data[] = array(
            'product_name'      => Input::get('product_name'),
            'quantity_in_stock' => Input::get('quantity_in_stock'),
            'price_per_item'    => Input::get('price_per_item'),
            'datetime'          => date('y-m-d H:i:s')
        );
        $new_data = json_encode($current_data);

        if(File::put($path, $new_data)) {
            return 'success';
        } else {
            return 'error';
        }

    }

}