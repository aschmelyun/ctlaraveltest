<!DOCYTYPE html>
<head>
    <title>Coalition Technologies Laravel Test</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <style>
        .row {
            margin-bottom: 12px;
        }
    </style>
</head>
<body>
<div class="container">
    @yield('content')
</div>
<script type="text/javascript" src="/assets/js/main.js"></script>
</body>
</html>