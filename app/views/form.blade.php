@extends('common.default')
@section('content')
<div class="row">
    <h3>Add A Product</h3>
    <div class="row">
        <div class="form-group">
            {{ Form::open(array('action' => 'PageController@saveForm', 'class' => 'product-data')) }}
            <div class="col-md-6">
                {{ Form::label('product_name', 'Product Name') }}
                {{ Form::text('product_name', '', array('class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('quantity_in_stock', 'Quantity In Stock') }}
                {{ Form::text('quantity_in_stock', '', array('class' => 'form-control')) }}
            </div>
            <div class="col-md-3">
                {{ Form::label('price_per_item', 'Price Per Item') }}
                {{ Form::text('price_per_item', '', array('class' => 'form-control')) }}
            </div>            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {{ Form::submit('Add Item', array('class' => 'btn btn-primary', 'id' => 'add_item')) }}
            {{ Form::close() }}
        </div>
    </div>
</div>
<div class="row">
    <h3>Product Data</h3>
    <div class="col-md-12">
        <table class="table table-striped" id="products_table">
            <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Quantity In Stock</th>
                    <th>Price Per Item</th>
                    <th>Datetime Submitted</th>
                    <th>Total Value #</th>
                </tr>
            </thead>
            <tbody>
                @foreach($product_json as $product)
                    <tr>
                        <td>{{ $product->product_name }}</td>
                        <td>{{ $product->quantity_in_stock }}</td>
                        <td>{{ $product->price_per_item }}</td>
                        <td>{{ $product->datetime }}</td>
                        <td>{{ (intval($product->quantity_in_stock) * floatval($product->price_per_item)) }}</td>
                    </tr>
                    <?php $total_value += (intval($product->quantity_in_stock) * floatval($product->price_per_item)); ?>
                @endforeach
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><b>Total Value:</b></td>
                        <td><b>{{ $total_value }}</b></td>
                    </tr>
            </tbody>
        </table>
    </div>
</div>
@stop